/******************************************************************************
 * Copyright 2022 The AIR Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef V2x_ASN_MSGS_ADAPTER_HPP
#define V2x_ASN_MSGS_ADAPTER_HPP

#include <string>

#include "v2x-asn-msgs-asn_2.h"
#include "v2x-asn-msgs-asn_3.h"

enum class EnAsnType : int32_t {
  CASE_53_2020,
  YDT_3709_2020,
};

inline ssize_t message_frame_uper2xer_adapter(const std::string& str_in,
                                              std::string* str_out,
                                              const EnAsnType& type) {
  if (!str_out) {
    return -1;
  }
  ssize_t res = -1;
  switch (type) {
    case EnAsnType::CASE_53_2020:
      res = v2x_asn_msg_asn_2_message_frame_uper2xer(str_in, str_out);
      return res;
    case EnAsnType::YDT_3709_2020:
      res = v2x_asn_msg_asn_3_message_frame_uper2xer(str_in, str_out);
      return res;
    default:
      break;
  }

  return res;
}

inline ssize_t message_frame_xer2uper_adapter(const std::string& str_in,
                                              std::string* str_out,
                                              const EnAsnType& type) {
  if (!str_out) {
    return -1;
  }
  ssize_t res = -1;
  switch (type) {
    case EnAsnType::CASE_53_2020:
      res = v2x_asn_msg_asn_2_message_frame_xer2uper(str_in, str_out);
      return res;
    case EnAsnType::YDT_3709_2020:
      res = v2x_asn_msg_asn_3_message_frame_xer2uper(str_in, str_out);
      return res;
    default:
      break;
  }

  return res;
}

inline ssize_t message_frame_uper2pbstr_adapter(const std::string& str_in,
                                                std::string* str_out,
                                                const EnAsnType& type) {
  if (!str_out) {
    return -1;
  }
  ssize_t res = -1;
  switch (type) {
    case EnAsnType::CASE_53_2020:
      res = v2x_asn_msg_asn_2_message_frame_uper2pbstr(str_in, str_out);
      return res;
    case EnAsnType::YDT_3709_2020:
      res = v2x_asn_msg_asn_3_message_frame_uper2pbstr(str_in, str_out);
      return res;
    default:
      break;
  }

  return res;
}

inline ssize_t message_frame_pbstr2uper_adapter(const std::string& str_in,
                                                std::string* str_out,
                                                const EnAsnType& type) {
  if (!str_out) {
    return -1;
  }
  ssize_t res = -1;
  switch (type) {
    case EnAsnType::CASE_53_2020:
      res = v2x_asn_msg_asn_2_message_frame_pbstr2uper(str_in, str_out);
      return res;
    case EnAsnType::YDT_3709_2020:
      res = v2x_asn_msg_asn_3_message_frame_pbstr2uper(str_in, str_out);
      return res;
    default:
      break;
  }

  return res;
}

inline ssize_t message_frame_map_xml2uper_adapter(const std::string& str_in,
                                                std::string* str_out,
                                                const EnAsnType& type) {
  if (!str_out) {
    return -1;
  }
  ssize_t res = -1;
  switch (type) {
    case EnAsnType::CASE_53_2020:
      res = v2x_asn_msg_asn_2_message_frame_map_xml2uper(str_in, str_out);
      return res;
    case EnAsnType::YDT_3709_2020:
      res = v2x_asn_msg_asn_3_message_frame_map_xml2uper(str_in, str_out);
      return res;
    default:
      break;
  }

  return res;
}

#endif
