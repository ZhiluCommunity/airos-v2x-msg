/******************************************************************************
 * Copyright 2022 The AIR Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <glog/logging.h>
#include "v2xpb-asn-message-frame.pb.h"
#include "MessageFrame.h"
#include <pugixml.hpp>

#if defined(CONFIG_ADAPTER_MODULE_asn_3_ENABLE)
#include "v2x-asn-msgs-asn_3.h"
#elif defined(CONFIG_ADAPTER_MODULE_asn_2_ENABLE)
#include "v2x-asn-msgs-asn_2.h"
#else
#error Internal Error NO module ENABLED
#endif

#include <GeographicLib/UTMUPS.hpp>
#include "asn1-holder.hpp"
//
constexpr int64_t V2X_ASN_ELEVATION_MAX = 61439;  // max value
constexpr int64_t V2X_ASN_ELEVATION_MIN = -4096;  // min value means unavailable
//
bool position_asn2pb(                 //
    const Position3D_t *pos_asn,      //
    ::v2xpb::asn::Position *position  //
);
bool position_asn2pb(                    //
    const Position3D_t *pos_center,      //
    const PositionOffsetLLV_t *pos_asn,  //
    ::v2xpb::asn::Position *position     //
);
//
bool position_pb2asn(                        //
    const ::v2xpb::asn::Position &position,  //
    Position3D_t *pos_asn                    //
);
bool position_pb2asn(                        //
    const Position3D_t *pos_center,          //
    const ::v2xpb::asn::Position &position,  //
    PositionOffsetLLV_t *pos_asn             //
);
//
bool bsm_asn2pb(const BasicSafetyMessage_t *bsm_asn, ::v2xpb::asn::Bsm *bsm_pb);
bool map_asn2pb(const MapData_t *map_asn, ::v2xpb::asn::Map *map_pb);
bool rsm_asn2pb(const RoadsideSafetyMessage_t *rsm_asn,
                ::v2xpb::asn::Rsm *rsm_pb);
bool spat_asn2pb(const SPAT_t *spat_asn, ::v2xpb::asn::Spat *spat_pb);
bool rsi_asn2pb(const RoadSideInformation_t *rsi_asn,
                ::v2xpb::asn::Rsi *rsi_pb);

bool bsm_pb2asn(const ::v2xpb::asn::Bsm &bsm_pb, BasicSafetyMessage_t *bsm_asn);
bool map_pb2asn(const ::v2xpb::asn::Map &map_pb, MapData_t *map_asn);
bool rsm_pb2asn(const ::v2xpb::asn::Rsm &rsm_pb,
                RoadsideSafetyMessage_t *rsm_asn);
bool spat_pb2asn(const ::v2xpb::asn::Spat &spat_pb, SPAT_t *spat_asn);
bool rsi_pb2asn(const ::v2xpb::asn::Rsi &rsi_pb,
                RoadSideInformation_t *rsi_asn);
bool map_xml2asn(const pugi::xml_document &map_xml, MapData_t *map_asn);
